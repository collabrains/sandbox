using namespace System.Net
param($Request, $TriggerMetadata)
Write-Host "PowerShell HTTP trigger function processed a request."
$name = $Request.Query.Name
if (-not $name) {
    $name = $Request.Body.Name
}
$body = [PSCustomObject]@{
    Name    = 'Chen'
    Id      = '1'
    Country = 'India'
    City    = 'Bangalore'
    Slot    = 'Staging'
} | ConvertTo-Json -Compress


# Associate values to output bindings by calling 'Push-OutputBinding'.
Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
        StatusCode = [HttpStatusCode]::OK
        Body       = $body
    })
