variable "resource_group_name" {
  type        = string
  description = "name of the resource group."
}


variable "location" {
  type        = string
  description = "resource location."
}

variable "storage_account_name" {
  type        = string
  description = "name of the storage account."
}

variable "storage_account_tier" {
  type        = string
  description = "storage account tier."
}

variable "storage_account_replication_type" {
  type        = string
  description = "storage account replication type."
}

variable "app_service_plan" {
  type        = string
  description = "app service plan."
}

variable "app_service_plan_tier" {
  type        = string
  description = "app service plan tier."
}

variable "app_service_plan_size" {
  type        = string
  description = "app service plan size."
}

variable "function_app_name" {
  type        = string
  description = "function app name."
}

variable "function_app_slot_name" {
  type = string
  description = "function app slot name!"
}