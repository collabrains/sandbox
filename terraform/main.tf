resource "azurerm_resource_group" "resource_group" {
  name     = var.resource_group_name
  location = var.location
}


resource "azurerm_storage_account" "storage_account" {
  name                     = var.storage_account_name
  resource_group_name      = azurerm_resource_group.resource_group.name
  location                 = azurerm_resource_group.resource_group.location
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_account_replication_type
}

resource "azurerm_service_plan" "windows_app_service_plan" {
  name                = var.app_service_plan
  resource_group_name = azurerm_resource_group.resource_group.name
  location            = azurerm_resource_group.resource_group.location
  os_type             = "Windows"
  sku_name            = "P1v2"
}

resource "azurerm_windows_function_app" "windows_function_app" {
  name                       = var.function_app_name
  resource_group_name        = azurerm_resource_group.resource_group.name
  location                   = azurerm_resource_group.resource_group.location
  storage_account_name       = azurerm_storage_account.storage_account.name
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  service_plan_id            = azurerm_service_plan.windows_app_service_plan.id
  site_config {
    application_stack {
      powershell_core_version = "7.2"
    }
    use_32_bit_worker = false
    always_on         = true
  }
  app_settings = {
    AzureWebJobsSecretStorageType = "Blob"
    AzureWebJobsStorage           = azurerm_storage_account.storage_account.primary_access_key
    FUNCTIONS_EXTENSION_VERSION   = "~4"
    FUNCTIONS_WORKER_RUNTIME      = "powershell"
  }
}
resource "azurerm_windows_function_app_slot" "windows_function_app_slot" {
  name                 = var.function_app_slot_name
  function_app_id      = azurerm_windows_function_app.windows_function_app.id
  storage_account_name = azurerm_storage_account.storage_account.name
  site_config {
    application_stack {
      powershell_core_version = "7.2"
    }
    use_32_bit_worker = false
    always_on         = true
  }
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  enabled                    = true
  app_settings = {
    AzureWebJobsSecretStorageType = "Blob"
    AzureWebJobsStorage           = azurerm_storage_account.storage_account.primary_access_key
    FUNCTIONS_EXTENSION_VERSION   = "~4"
    FUNCTIONS_WORKER_RUNTIME      = "powershell"
    WEBSITE_RUN_FROM_PACKAGE      = 1
  }
}
